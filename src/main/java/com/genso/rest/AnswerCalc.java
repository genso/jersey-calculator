package com.genso.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AnswerCalc {
    private String call = "";
    private Double result = 0.0;
    private String message = "";


    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}