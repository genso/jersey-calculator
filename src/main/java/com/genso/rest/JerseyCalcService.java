package com.genso.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.*;
import javax.ws.rs.Produces;


@Path("/calc")
public class JerseyCalcService {

    @GET
    @Produces({MediaType.APPLICATION_JSON })
	@Path("/add/{numbers:.*}")
    public Response addNumbers(@PathParam("numbers") String numbers, @Context Request request) {
        String[] numbersString = numbers.split("/");
        double[] numbersInteger = new double[numbersString.length];
        String message = "Not a valid number: ";
        Double suma = 0.0;

        for(int i = 0;i < numbersString.length;i++){
            //match a number with optional '-' and decimal.
            if(numbersString[i].matches("-?\\d+(\\.\\d+)?")){
                numbersInteger[i] = Double.parseDouble(numbersString[i]);
            }
            else{
                numbersInteger[i] = 0;
                message += numbersString[i];
            }
        }

        //Show message
        if(message.length() == 20)
            message = " ";

        //CACHE
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86400);

        //There are arguments
        if(numbersInteger.length > 1){
            suma = sum(numbersInteger);
        }
        //No arguments
        else{
            message = "No arguments";
        }

        EntityTag etag = new EntityTag(Double.toString(suma.hashCode()));
        Response.ResponseBuilder builder = request.evaluatePreconditions(etag);

        // cached resource did change -> serve updated content
        if(builder == null){
            AnswerCalc msg = new AnswerCalc();
            msg.setCall("Add");
            msg.setResult(suma);
            msg.setMessage(message);
            builder = Response.ok(msg);
            builder.tag(etag);
        }

        builder.cacheControl(cc);
        return builder.build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON })
	@Path("/subtract/{numbers:.*}")
    public Response subtractNumbers(@PathParam("numbers") String numbers, @Context Request request) {
        String[] numbersString = numbers.split("/");
        double[] numbersInteger = new double[numbersString.length];
        String message = "Not a valid number: ";
        Double resta = 0.0;

        for(int i = 0;i < numbersString.length;i++){
            //match a number with optional '-' and decimal.
            if(numbersString[i].matches("-?\\d+(\\.\\d+)?")){
                numbersInteger[i] = Double.parseDouble(numbersString[i]);
            }
            else{
                numbersInteger[i] = 0;
                message += numbersString[i];
            }
        }

        //Show message
        if(message.length() == 20)
            message = " ";

        //CACHE
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86400);

        //There are arguments
        if(numbersInteger.length > 1){
            resta = subtract(numbersInteger);
        }
        //No arguments
        else{
            message = "No arguments";
        }

        EntityTag etag = new EntityTag(Double.toString(resta.hashCode()));
        Response.ResponseBuilder builder = request.evaluatePreconditions(etag);

        // cached resource did change -> serve updated content
        if(builder == null){
            AnswerCalc msg = new AnswerCalc();
            msg.setCall("Subtract");
            msg.setResult(resta);
            msg.setMessage(message);
            builder = Response.ok(msg);
            builder.tag(etag);
        }

        builder.cacheControl(cc);
        return builder.build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON })
	@Path("/multiply/{numbers:.*}")
    public Response multiplyNumbers(@PathParam("numbers") String numbers, @Context Request request) {
        String[] numbersString = numbers.split("/");
        double[] numbersInteger = new double[numbersString.length];
        String message = "Not a valid number: ";
        Double multiplicacion = 0.0;

        for(int i = 0; i < numbersString.length; i++){
            //match a number with optional '-' and decimal.
            if(numbersString[i].matches("-?\\d+(\\.\\d+)?")){
                numbersInteger[i] = Double.parseDouble(numbersString[i]);
            }
            else{
                numbersInteger[i] = 1;
                message += numbersString[i];
            }
        }

        //Show message
        if(message.length() == 20)
            message = " ";

        //CACHE
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86400);

        //There are arguments
        if(numbersInteger.length > 1){
            multiplicacion = multiply(numbersInteger);
        }
        //No arguments
        else{
            multiplicacion = 0.0;
            message = "No arguments";
        }

        EntityTag etag = new EntityTag(Double.toString(multiplicacion.hashCode()));
        Response.ResponseBuilder builder = request.evaluatePreconditions(etag);

        // cached resource did change -> serve updated content
        if(builder == null){
            AnswerCalc msg = new AnswerCalc();
            msg.setCall("Multiply");
            msg.setResult(multiplicacion);
            msg.setMessage(message);
            builder = Response.ok(msg);
            builder.tag(etag);
        }

        builder.cacheControl(cc);
        return builder.build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON })
    @Path("/divide/{a}/{b}")
    public Response divideNumbers(@PathParam("a") String a, @PathParam("b") String b, @Context Request request) {
        Double divide = 0.0;
        AnswerCalc output = new AnswerCalc();

        if(a.matches("-?\\d+(\\.\\d+)?") && b.matches("-?\\d+(\\.\\d+)?")){
            double param1 = Double.parseDouble(a);
            double param2 = Double.parseDouble(b);
            if(param2 > 0){
                divide = divide(param1,param2);
                output.setCall("Divide");
                output.setResult(divide);
                output.setMessage(" ");
            }
            else{
                output.setCall("Divide");
                output.setResult(divide);
                output.setMessage("Can not divide by 0");
            }
        }
        else{
            String temp = "Not a valid number: ";
            if(a.matches("-?\\d+(\\.\\d+)?") == false){
                temp += a + " ";
            }
            if(b.matches("-?\\d+(\\.\\d+)?") == false){
                temp += b;
            }
            output.setCall("Divide");
            output.setResult(0.0);
            output.setMessage(temp);
        }

        //CACHE
        CacheControl cc = new CacheControl();
        cc.setMaxAge(86400);

        Double division = divide;

        EntityTag etag = new EntityTag(Double.toString(division.hashCode()));
        Response.ResponseBuilder builder = request.evaluatePreconditions(etag);

        // cached resource did change -> serve updated content
        if(builder == null){
            builder = Response.ok(output);
            builder.tag(etag);
        }

        builder.cacheControl(cc);
        return builder.build();
    }


    public double sum(double [] numbersArray)
    {
        double result=0;
        for(int i=0; i<numbersArray.length; i++)
        {
            result += numbersArray[i];
        }
        return result;
    }

    public double subtract(double [] numbersArray)
    {
        double result = numbersArray[0];
        for(int i=1; i<numbersArray.length; i++)
        {
            result -= numbersArray[i];
        }
        return result;
    }

    public double multiply(double [] numbersArray)
    {
        double result = 1;

        for(int i=0; i<numbersArray.length; i++)
        {
            result = result * numbersArray[i];
        }

        return result;
    }

    public double divide(double a, double b)
    {
        double result = a/b;
        return result;
    }
}



