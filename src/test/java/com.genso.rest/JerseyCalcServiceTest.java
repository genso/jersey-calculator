package com.genso.rest;

import static org.junit.Assert.assertEquals;

import java.net.URISyntaxException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.AppDescriptor;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;

public class JerseyCalcServiceTest extends JerseyTest {
    @Override
    protected AppDescriptor configure() {
        return new WebAppDescriptor.Builder().build();
    }

    @Test
    public void testAddNumbers() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/add/2/3/4").get(JSONObject.class);

        assertEquals("Add", json.get("call"));
        assertEquals(" ", json.get("message"));
        assertEquals("9.0", json.get("result"));
    }

    @Test
    public void testAddNoValid() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/add/2/b/4").get(JSONObject.class);

        assertEquals("Add", json.get("call"));
        assertEquals("Not a valid number: b", json.get("message"));
        assertEquals("6.0", json.get("result"));
    }

    @Test
    public void testAddNoArguments() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/add/").get(JSONObject.class);

        assertEquals("Add", json.get("call"));
        assertEquals("No arguments", json.get("message"));
        assertEquals("0.0", json.get("result"));
    }

    @Test
    public void testSubtractNumbers() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/subtract/2/3/4").get(JSONObject.class);

        assertEquals("Subtract", json.get("call"));
        assertEquals(" ", json.get("message"));
        assertEquals("-5.0", json.get("result"));
    }

    @Test
    public void testSubtractNoValid() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/subtract/4/b/2").get(JSONObject.class);

        assertEquals("Subtract", json.get("call"));
        assertEquals("Not a valid number: b", json.get("message"));
        assertEquals("2.0", json.get("result"));
    }

    @Test
    public void testSubtractNoArguments() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/subtract/").get(JSONObject.class);

        assertEquals("Subtract", json.get("call"));
        assertEquals("No arguments", json.get("message"));
        assertEquals("0.0", json.get("result"));
    }

    @Test
    public void testMultiplyNumbers() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/multiply/2/3/4").get(JSONObject.class);

        assertEquals("Multiply", json.get("call"));
        assertEquals(" ", json.get("message"));
        assertEquals("24.0", json.get("result"));
    }

    @Test
    public void testMultiplyNoValid() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/multiply/2/b/4").get(JSONObject.class);

        assertEquals("Multiply", json.get("call"));
        assertEquals("Not a valid number: b", json.get("message"));
        assertEquals("8.0", json.get("result"));
    }

    @Test
    public void testMultiplyNoArguments() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/multiply/").get(JSONObject.class);

        assertEquals("Multiply", json.get("call"));
        assertEquals("No arguments", json.get("message"));
        assertEquals("0.0", json.get("result"));
    }

    @Test
    public void testDivideNumbers() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/divide/8/4").get(JSONObject.class);

        assertEquals("Divide", json.get("call"));
        assertEquals(" ", json.get("message"));
        assertEquals("2.0", json.get("result"));
    }

    @Test
    public void testDivideNoValid() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/divide/2/b").get(JSONObject.class);

        assertEquals("Divide", json.get("call"));
        assertEquals("Not a valid number: b", json.get("message"));
        assertEquals("0.0", json.get("result"));
    }

    @Test
    public void testDivideByZero() throws JSONException, URISyntaxException {
        WebResource webResource = client().resource("http://localhost:8080/");
        JSONObject json = webResource.path("com.genso.rest/rest/calc/divide/2/0").get(JSONObject.class);

        assertEquals("Divide", json.get("call"));
        assertEquals("Can not divide by 0", json.get("message"));
        assertEquals("0.0", json.get("result"));
    }
}
