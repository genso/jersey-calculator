# README #

Jersey calculator

### What is this repository for? ###

* App for test Jersey
* Version 0.1 Snapshot

### How do I get set up? ###

* 
* Summary of set up
* Clone repository: git clone https://andrea_genso@bitbucket.org/genso/jersey-calculator.git
* use maven3 for deploy:
```
#!script

mvn clean
``` 
```
#!script

mvn compile
```
```
#!script

mvn package -DskipTests
```

* copy .war file generated to your Tomat server in webapps folder
```
#!script

mvn test
```


* How to run tests
```
#!script

mvn test
```
or you can test on browser with next examples:
```
#!script

[host]:[port]/com.genso.rest/rest/calc/add/2/3/4
[host]:[port]/com.genso.rest/rest/calc/add/2/b/4
[host]:[port]/com.genso.rest/rest/calc/subtract/2/3/4
[host]:[port]/com.genso.rest/rest/calc/subtract/4/b/2
[host]:[port]/com.genso.rest/rest/calc/subtract/
[host]:[port]/com.genso.rest/rest/calc/multiply/2/3/4
[host]:[port]/com.genso.rest/rest/calc/multiply/2/b/4
[host]:[port]/com.genso.rest/rest/calc/multiply/
[host]:[port]/com.genso.rest/rest/calc/divide/8/4
[host]:[port]/com.genso.rest/rest/calc/divide/2/b
[host]:[port]/com.genso.rest/rest/calc/divide/2/0
```


### Who do I talk to? ###

* info@genso.com.bo